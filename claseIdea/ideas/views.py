from django.shortcuts import render

# Create your views here.
from ideas.models import TipoIdea, Idea


def cargar_idea(request):
    if request.method == 'POST':
        nueva_idea = request.POST.get('idea')
        tipo_idea_id = request.POST.get('tipo')
        idea = Idea()
        idea.descripcion = nueva_idea
        idea.tipo = TipoIdea.objects.get(id=tipo_idea_id)
        idea.save()

    tipos_ideas = TipoIdea.objects.all()
    ideas_cargadas = Idea.objects.all()
    return render(request, 'ideas.html', {'tipos': tipos_ideas,'ideas_cargadas':ideas_cargadas })


def cargar_tipo_idea(request):
    if request.method == 'POST':
        tipo_idea = request.POST.get('tipo_idea')
        nuevo_tipo = TipoIdea()
        nuevo_tipo.descripcion = tipo_idea
        nuevo_tipo.save()

    tipos_ideas = TipoIdea.objects.all()
    return render(request, 'tipo_idea.html', {'tipos': tipos_ideas})
