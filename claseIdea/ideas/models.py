from django.db import models

# Create your models here.




class TipoIdea(models.Model):
    descripcion = models.CharField(max_length=100)

class Idea(models.Model):
    descripcion = models.CharField(max_length=100)
    tipo = models.ForeignKey(TipoIdea,on_delete=models.CASCADE)
